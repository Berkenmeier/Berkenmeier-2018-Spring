package com.example.nate.afinal;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

public class ShelterYouTubePlayer extends YouTubeBaseActivity {
    YouTubePlayerView youTubePlayerView;
    Button button;
    YouTubePlayer.OnInitializedListener onInitializedListener;
    TextView spotDescription;

    @Override
    protected  void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shelter);
        Intent theIntent = new Intent(this, ShelterYouTubePlayer.class);
        int spot = theIntent.getIntExtra("Spot",999);
        spotDescription = (TextView)findViewById(R.id.spotDescription);
        button = (Button)findViewById(R.id.bn);
        youTubePlayerView = (YouTubePlayerView)findViewById(R.id.youtube_player_view);

            onInitializedListener = new YouTubePlayer.OnInitializedListener() {
                @Override
                public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean bool) {
                    youTubePlayer.loadVideo("usJ2YARrSzQ");
                }
                @Override
                public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

                }
            };

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                youTubePlayerView.initialize(MainActivity.API_KEY, onInitializedListener);
            }
        });

    }

}
