package com.example.nate.afinal

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide

class Weapons : AppCompatActivity() {

    lateinit var weaponsMain:LinearLayout
    lateinit var assaultRifles:TextView
    lateinit var sniperRifles:TextView
    lateinit var shotguns:TextView
    lateinit var crateWeapons:TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_weapons)

        val weaponsMain = findViewById<LinearLayout>(R.id.weaponsMain)
        val assaultRifles = findViewById<TextView>(R.id.aRifle)
        val sniperRifles = findViewById<TextView>(R.id.sRifle)
        val shotguns = findViewById<TextView>(R.id.shotty)
        val crateWeapons = findViewById<TextView>(R.id.crate)

        val weaponsImage = ImageView(this)
        weaponsMain.addView(weaponsImage)
        Glide.with(this).load("http://cdn1-www.gamerevolution.com/assets/uploads/2018/02/is-pubg-free.jpg").into(weaponsImage)

        assaultRifles.setOnClickListener {
            val intent: Intent = Intent(this, AssaultRifles::class.java)
            startActivityForResult(intent, 12)
        }
        sniperRifles.setOnClickListener {
            val intent: Intent = Intent(this, ComingSoon::class.java)
            startActivityForResult(intent, 12)
        }
        shotguns.setOnClickListener {
            val intent: Intent = Intent(this, ComingSoon::class.java)
            startActivityForResult(intent, 12)
        }
        crateWeapons.setOnClickListener {
            val intent: Intent = Intent(this, ComingSoon::class.java)
            startActivityForResult(intent, 12)
        }
    }
}
