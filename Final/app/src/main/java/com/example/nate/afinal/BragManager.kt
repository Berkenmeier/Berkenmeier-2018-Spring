package com.example.nate.afinal

import android.graphics.Bitmap

object BragManager {
    val PicList = mutableListOf<Bitmap>()
    val NameList = mutableListOf<String>()

    fun addPic(pic: Bitmap, key:Int) {
        PicList.add(key, pic)
    }
    fun getPic(index:Int): Bitmap {
        return PicList.get(index)
    }
    fun addData(name:String, index: Int) {
        NameList.add(index, name)
    }
    fun getData(index:Int): String {
        return NameList.get(index)
    }

    fun returnPicList(): MutableList<Bitmap> {
        return PicList
    }
}