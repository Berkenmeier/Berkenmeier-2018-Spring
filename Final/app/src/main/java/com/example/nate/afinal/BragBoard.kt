package com.example.nate.afinal

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.provider.MediaStore
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import com.bumptech.glide.Glide
import org.jetbrains.anko.db.insert
import java.io.ByteArrayOutputStream

class BragBoard : AppCompatActivity() {

    lateinit var camera:Button
    lateinit var dinner1:ImageView
    lateinit var dinner2:ImageView
    lateinit var dinner3:ImageView
    lateinit var dinner5:ImageView
    lateinit var dinner6:ImageView
    lateinit var dinner7:ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_brag_board)
        val dinnerLayout = findViewById<LinearLayout>(R.id.chickenDinner)
        val dinnerImage = ImageView(this)
        val dinner1 = findViewById<ImageView>(R.id.dinner1)
        val dinner2 = findViewById<ImageView>(R.id.dinner2)
        val dinner5 = findViewById<ImageView>(R.id.dinner5)
        val dinner6 = findViewById<ImageView>(R.id.dinner6)
        val dinner7 = findViewById<ImageView>(R.id.dinner7)
        dinnerLayout.addView(dinnerImage)
        Glide.with(this).load("https://image.spreadshirtmedia.com/image-server/v1/mp/products/T812A2MPA1663PT17X79Y37D1012141713S22/views/1,width=1200,height=630,appearanceId=2,backgroundColor=E8E8E8,modelId=115,crop=design,version=1515071979/pubg-winner-winner-chicken-dinner-men-s-premium-t-shirt.jpg").into(dinnerImage)
        Glide.with(this).load("http://i64.tinypic.com/2s8hdle.jpg").into(dinner1)
        Glide.with(this).load("https://pbs.twimg.com/media/C_J_7bsUwAAyWWo.jpg").into(dinner2)
        Glide.with(this).load("https://i.ytimg.com/vi/SREXrqV1w7Y/maxresdefault.jpg").into(dinner5)
        Glide.with(this).load("https://i.ytimg.com/vi/EQqr67DJNPE/maxresdefault.jpg").into(dinner6)
        Glide.with(this).load("https://i.ytimg.com/vi/8Ol1zUv6gik/maxresdefault.jpg").into(dinner7)
        val db:MyDatabaseOpenHelper
        val camera: Button = findViewById<Button>(R.id.camera)
        var count:Int = 0

        camera.setOnClickListener {

            val cameraCheckPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA )

            if( cameraCheckPermission != PackageManager.PERMISSION_GRANTED){
                if( ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) == true){
                    val builder = AlertDialog.Builder(this)

                    builder.setMessage("I need to see you to work properly!!")
                            .setTitle("Permission required")
                            .setPositiveButton("OK") { dialog, id ->
                                requestPermission()
                            }
                    val dialog = builder.create()
                    dialog.show()

                }
                else{
                    requestPermission()

                }
            }
            else{

                launchCamera(count)
                count++
                if (count==4) {
                    count = 0
                }

            }

        }

    }

    private fun launchCamera(count:Int){

        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra("count", count)
        startActivityForResult(intent, 9090)
        if(count == 1) {
            startActivityForResult(intent, 9091)
        }



    }
    private fun requestPermission(){
        ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.CAMERA), 765)

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        for ((index, permission) in permissions.withIndex()){
            if( permission == Manifest.permission.CAMERA){
                if( grantResults[index] == PackageManager.PERMISSION_GRANTED){
                    //launchCamera()
                }
            }
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == 9090){
            if( data != null ) {
                val imageData: Bitmap = data.extras.get("data") as Bitmap
                val insert:ByteArray = convert(imageData)
                val dinner3:ImageView = findViewById(R.id.dinner3)
                dinner3.setImageBitmap(imageData)
                BragManager.addPic(imageData, 0)

                database.use {
                    insert("Dinner",
                            "id" to 0,
                            "name" to "dummy",
                            "photo" to insert
                    )
                }
            }
        }
        if(requestCode == 9091){
            if( data != null ) {
                val imageData: Bitmap = data.extras.get("data") as Bitmap
                val insert:ByteArray = convert(imageData)
                val dinner4:ImageView = findViewById(R.id.dinner4)
                dinner4.setImageBitmap(imageData)
                BragManager.addPic(imageData, 1)

                database.use {
                    insert("Dinner",
                            "id" to 1,
                            "name" to "dummy",
                            "photo" to insert
                    )
                }
            }
        }
    }

    private fun convert(image: Bitmap): ByteArray {
        val stream = ByteArrayOutputStream()
        image.compress(Bitmap.CompressFormat.PNG, 90, stream)
        return stream.toByteArray()
    }

    }

