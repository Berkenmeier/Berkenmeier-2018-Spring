package com.example.nate.afinal

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide

class Map : AppCompatActivity() {

    lateinit var shelterText:TextView
    lateinit var nextShelter:TextView
    lateinit var school:TextView
    lateinit var nextSchool:TextView
    lateinit var mtc:TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)

        shelterText = findViewById(R.id.shelterText)
        nextShelter = findViewById(R.id.nextShelter)
        school = findViewById(R.id.school)
        nextSchool = findViewById(R.id.nextSchool)
        mtc = findViewById(R.id.mtcMap)

        val mapLayout = findViewById<LinearLayout>(R.id.mapLayout)
        val mapImage = ImageView(this)
        mapLayout.addView(mapImage)
        Glide.with(this).load("https://pro-rankedboost.netdna-ssl.com/wp-content/uploads/2017/06/Playerunknown-Battlegrounds-Gun-Locations-and-Vehicle-Spawns2.jpg").into(mapImage)

        shelterText.setOnClickListener {
            val intent: Intent = Intent(this, ShelterYouTubePlayer::class.java)
            intent.putExtra("Spot", 0)
            startActivityForResult(intent, 12)
        }
        nextShelter.setOnClickListener {
            val intent: Intent = Intent(this, ShelterYouTubePlayer::class.java)
            intent.putExtra("Spot", 0)
            startActivityForResult(intent, 12)
        }
        school.setOnClickListener {
            val intent: Intent = Intent(this, ComingSoon::class.java)
            intent.putExtra("Spot", 1)
            startActivityForResult(intent, 12)
        }
        nextSchool.setOnClickListener {
            val intent: Intent = Intent(this, ComingSoon::class.java)
            intent.putExtra("Spot", 1)
            startActivityForResult(intent, 12)
        }
        mtc.setOnClickListener {
            val intent: Intent = Intent(this, ComingSoon::class.java)
            startActivityForResult(intent, 12)
        }

    }
}
