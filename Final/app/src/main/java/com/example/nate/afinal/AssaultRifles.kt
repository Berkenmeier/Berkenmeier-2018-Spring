package com.example.nate.afinal

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide

class AssaultRifles : AppCompatActivity() {

    lateinit var scar:TextView
    lateinit var m16:TextView
    lateinit var m4:TextView
    lateinit var akm:TextView
    lateinit var weaponDescription:TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_assault_rifles)

        scar = findViewById(R.id.scar)
        m16 = findViewById(R.id.m16)
        m4 = findViewById(R.id.m4)
        akm = findViewById(R.id.akm)
        weaponDescription = findViewById(R.id.assaultMessage)
        val ARLayout = findViewById<LinearLayout>(R.id.assaultPicLayout)
        val arImage = ImageView(this)
        ARLayout.addView(arImage)
        Glide.with(this).load("https://pbs.twimg.com/media/DZRbvz0X4AA7EBE.jpg").into(arImage)
        scar.setOnClickListener {
            val scarImage = ImageView(this)
            ARLayout.removeAllViews()
            ARLayout.addView(scarImage)
            Glide.with(this).load("https://d1fs8ljxwyzba6.cloudfront.net/assets/editorial/2017/04/battlegrounds-SCAR-L-assault-rifle.jpg").into(scarImage)
            weaponDescription.text = ARManager.scar()
        }
        m16.setOnClickListener {
            val m16Image = ImageView(this)
            ARLayout.removeAllViews()
            ARLayout.addView(m16Image)
            Glide.with(this).load("https://steamuserimages-a.akamaihd.net/ugc/845963477077743018/4A08BDF9A810197C5831CEE1AF64BA01D33C0EDE/").into(m16Image)
            weaponDescription.text = ARManager.m16()
        }
        m4.setOnClickListener {
            val m4Image = ImageView(this)
            ARLayout.removeAllViews()
            ARLayout.addView(m4Image)
            Glide.with(this).load("https://d1fs8ljxwyzba6.cloudfront.net/assets/editorial/2017/04/battlegrounds-M416_German-assault-rifle.jpg").into(m4Image)
            weaponDescription.text = ARManager.m4()
        }
        akm.setOnClickListener {
            val akmImage = ImageView(this)
            ARLayout.removeAllViews()
            ARLayout.addView(akmImage)
            Glide.with(this).load("http://upload2.inven.co.kr/upload/2017/07/29/bbs/i14027321944.png").into(akmImage)
            weaponDescription.text = ARManager.akm()
        }
    }
}
