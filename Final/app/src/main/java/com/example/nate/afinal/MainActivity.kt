package com.example.nate.afinal

import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    companion object {
        const val USER_KEY:String = "user"
        const val API_KEY:String = "AIzaSyC3LUUqWVF9Ol1tnz-zaWbDSrfkCNhNnkw"
    }

    lateinit var send:Button
    lateinit var gamertag:EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        val pubgLayout = findViewById<LinearLayout>(R.id.PUBGLayout)
        val pubgImage = ImageView(this)
        pubgLayout.addView(pubgImage)
        Glide.with(this).load("https://techcrunch.com/wp-content/uploads/2018/01/pubg.gif?w=1390&crop=1").into(pubgImage)
        val nextLayout = findViewById<LinearLayout>(R.id.nextLayout)
        val nextImage = ImageView(this)
        nextLayout.addView(nextImage)
        Glide.with(this).load("https://techcrunch.com/wp-content/uploads/2018/01/pubg.gif?w=1390&crop=1").into(nextImage)

        var send = findViewById<Button>(R.id.webViewSend)
        var gamertag = findViewById<EditText>(R.id.gamerText)



        send.setOnClickListener {
            var dynamicUser:String = gamertag.text.toString()
            val intent: Intent = Intent(this, WebView::class.java)
            intent.putExtra(USER_KEY, dynamicUser)
            startActivityForResult(intent, 12)
        }

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.weapons -> {
                val intent: Intent = Intent(this, Weapons::class.java)
                startActivityForResult(intent, 12)
            }
            R.id.map -> {
                val intent: Intent = Intent(this, Map::class.java)
                startActivityForResult(intent, 12)
            }
            R.id.bragBoard -> {
                val intent: Intent = Intent(this, BragBoard::class.java)
                startActivityForResult(intent, 12)
            }
            R.id.more -> {
                val intent: Intent = Intent(this, ComingSoon::class.java)
                startActivityForResult(intent, 12)
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}
