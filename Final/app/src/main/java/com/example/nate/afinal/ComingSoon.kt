package com.example.nate.afinal

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

class ComingSoon : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_coming_soon)
    }
}
