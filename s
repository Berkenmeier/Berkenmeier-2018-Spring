[33mcommit 530e4f0c7034183c3115cdfb83df7477ed3d5232[m[33m ([m[1;36mHEAD[m[33m)[m
Author: NarthVader <nathanberkenmeier@u.boisestate.edu>
Date:   Thu Apr 12 10:53:58 2018 -0600

    complete P2 revamp

[33mcommit c6670acc46d2d22516c9f743b7b1fc394507a4b3[m
Author: NarthVader <nathanberkenmeier@u.boisestate.edu>
Date:   Thu Apr 12 00:04:58 2018 -0600

    broken recycler view code

[33mcommit 253910dc99879fd4fe601f256591bd8fc36aa425[m
Author: NarthVader <nathanberkenmeier@u.boisestate.edu>
Date:   Wed Apr 11 23:34:38 2018 -0600

    revert

[33mcommit 1ed51a1bbf0ae37d70bdaf4236501e167b5dcadb[m
Author: NarthVader <nathanberkenmeier@u.boisestate.edu>
Date:   Wed Apr 11 23:33:44 2018 -0600

    recycler view fail

[33mcommit 91e30ae7bf9e9c12bc2da525ecdc034f22ac28e6[m
Author: NarthVader <nathanberkenmeier@u.boisestate.edu>
Date:   Wed Apr 11 22:39:39 2018 -0600

    implement fragment interaction listener

[33mcommit b285ef5ab6add4e2760ca84f0ab5b004c3066737[m
Author: NarthVader <nathanberkenmeier@u.boisestate.edu>
Date:   Wed Apr 11 22:36:13 2018 -0600

    rid of old view code

[33mcommit 7b181d604ecdda69602eff62bdd8a3d741962c71[m
Author: NarthVader <nathanberkenmeier@u.boisestate.edu>
Date:   Wed Apr 11 22:32:18 2018 -0600

    add first, then replace

[33mcommit 85f3011d0f9d6c0762fef9222c11de92569cfc1d[m
Author: NarthVader <nathanberkenmeier@u.boisestate.edu>
Date:   Wed Apr 11 22:30:11 2018 -0600

    fragment manager

[33mcommit 0af47de363c24805b26c841d9106912c5431f2ca[m
Author: NarthVader <nathanberkenmeier@u.boisestate.edu>
Date:   Wed Apr 11 22:15:30 2018 -0600

    recycler fragment added

[33mcommit d7eea2de53516532042a4a6f1df8965e0e8f5472[m
Author: NarthVader <nathanberkenmeier@u.boisestate.edu>
Date:   Tue Apr 10 19:07:54 2018 -0600

    got rid of broken recycler view code in P1 bc I noticed it wasn't graded yet

[33mcommit 10fa9a3cff16211a013e02114b7575212231cfbb[m
Author: NarthVader <nathanberkenmeier@u.boisestate.edu>
Date:   Tue Apr 10 18:55:54 2018 -0600

    referenced the proper counter to add to correct index in db

[33mcommit d4c1220e59bd8431d9745a1c64c81fc97bc45a36[m
Author: NarthVader <nathanberkenmeier@u.boisestate.edu>
Date:   Tue Apr 10 18:54:43 2018 -0600

    got a proper add to database in main activity, also modified the update in pic activity

[33mcommit bc3b4048063a20cfe2c64e2924e2d21682012454[m
Author: NarthVader <nathanberkenmeier@u.boisestate.edu>
Date:   Tue Apr 10 18:38:18 2018 -0600

    awesome convert bitmap to byte array function added to get bitmap into sqlite db!!!

[33mcommit 72c23331e97ca4871ad8b59d02f69995404e9b15[m
Author: NarthVader <nathanberkenmeier@u.boisestate.edu>
Date:   Tue Apr 10 18:22:58 2018 -0600

    return PicList object

[33mcommit 1b2fef126704db17e48ab1191b357167a637bd3f[m
Author: NarthVader <nathanberkenmeier@u.boisestate.edu>
Date:   Tue Apr 10 17:00:59 2018 -0600

    added HW3 .pdf

[33mcommit c7e645e4991ccd4041b8e0ad988e34473cf800bc[m
Author: NarthVader <nathanberkenmeier@u.boisestate.edu>
Date:   Sat Apr 7 15:22:44 2018 -0600

    fragment work, TODO: get pics to populate recycler view

[33mcommit d04267ed974e37b503f3a2a8c8fe5b658da4f7ca[m
Author: NarthVader <nathanberkenmeier@u.boisestate.edu>
Date:   Sat Apr 7 14:39:31 2018 -0600

    basic fragment

[33mcommit 63cd24de33fc551e1d1bf37476b01b6fcdb20e86[m
Author: NarthVader <nathanberkenmeier@u.boisestate.edu>
Date:   Sat Apr 7 14:08:26 2018 -0600

    P2 file copy

[33mcommit 3c5c7a24f46dff9e7a13245f4f8de5864dcbe4f0[m
Author: NarthVader <nathanberkenmeier@u.boisestate.edu>
Date:   Tue Apr 3 15:56:52 2018 -0600

    update pic object in database added

[33mcommit bad10766a93c374423a4ec735eace6ed0eb92955[m
Author: NarthVader <nathanberkenmeier@u.boisestate.edu>
Date:   Tue Apr 3 15:45:19 2018 -0600

    added primary key id to contact added

[33mcommit 6a9ca9548c402b8f818e99d833501b9e43fc4b6f[m
Author: NarthVader <nathanberkenmeier@u.boisestate.edu>
Date:   Tue Apr 3 15:43:17 2018 -0600

    add contact code added

[33mcommit 37795603f2b60a5b883807a7b735cd16115125b1[m
Author: NarthVader <nathanberkenmeier@u.boisestate.edu>
Date:   Tue Apr 3 15:06:20 2018 -0600

    added database for Project 2

[33mcommit 767a78f214d209a91963c7c9f3a20d8814b0ff45[m[33m ([m[1;33mtag: P1FinishedAndWorking!!![m[33m)[m
Author: NarthVader <nathanberkenmeier@u.boisestate.edu>
Date:   Wed Mar 7 21:20:44 2018 -0700

    P1 finished and debugged!!!

[33mcommit 173d4671129dea8e0609efb047c2ec6b72b32c79[m
Author: NarthVader <nathanberkenmeier@u.boisestate.edu>
Date:   Tue Mar 6 16:21:28 2018 -0700

    new P1 dir

[33mcommit 332da2f9b04078e4fc2ab8289ded59352493de5a[m
Author: NarthVader <nathanberkenmeier@u.boisestate.edu>
Date:   Sun Mar 4 22:14:18 2018 -0700

    fixed onClick to pass text proper. next is intent passing strings. next
    is camera intents

[33mcommit b2dcc2a333e367b4498f69e6c1d496797970a113[m
Author: NarthVader <nathanberkenmeier@u.boisestate.edu>
Date:   Sun Mar 4 22:11:30 2018 -0700

    broken

[33mcommit 42b900b18f7c7346b85810c111956a34f2dffc52[m
Author: NarthVader <nathanberkenmeier@u.boisestate.edu>
Date:   Sun Mar 4 20:51:13 2018 -0700

    first commit "hello world"

[33mcommit decf55a98b32cea56f0bd0cfe25f970a22ca5fc6[m
Author: NarthVader <nathanberkenmeier@u.boisestate.edu>
Date:   Sun Mar 4 13:31:00 2018 -0700

    HW2 PDF commit

[33mcommit 3a2b5e18f06a8daaa13d24070fcaa9ea856faebc[m
Author: NarthVader <nathanberkenmeier@u.boisestate.edu>
Date:   Tue Jan 23 23:46:57 2018 -0700

    Removed redundancy

[33mcommit cac2f43154644dd183f8010244fe770f5c35b3b7[m
Author: NarthVader <nathanberkenmeier@u.boisestate.edu>
Date:   Tue Jan 23 18:59:45 2018 -0700

    HW1 finished with cool Kotlin workarounds. Old code exists in comments
    above in the add.OnClick handler!

[33mcommit 302e2abe73726ed8e949d2b0d9c54575968c08ae[m
Author: NarthVader <nathanberkenmeier@u.boisestate.edu>
Date:   Tue Jan 23 14:48:22 2018 -0700

    HW1 with some toInt() modification

[33mcommit 0f22eae2dd715161b54829700719fb758e61a2da[m
Author: NarthVader <nathanberkenmeier@u.boisestate.edu>
Date:   Sat Jan 20 14:28:29 2018 -0700

    fixed some stuff

[33mcommit 17327f0ddd466336318f275fbe62edea4ed1e117[m
Author: NarthVader <nathanberkenmeier@u.boisestate.edu>
Date:   Sat Jan 20 14:18:06 2018 -0700

    HW1

[33mcommit be465003c78a5d68f8c34f6713c99ca46da6d146[m
Author: NarthVader <nathanberkenmeier@u.boisestate.edu>
Date:   Sat Jan 20 11:48:18 2018 -0700

    initial commit
