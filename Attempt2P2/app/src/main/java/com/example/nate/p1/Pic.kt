package com.example.nate.p1

/**
 * Created by Nate on 3/6/2018.
 */
import android.graphics.Bitmap
import android.widget.ImageView
import java.io.Serializable
import java.util.*

data class Pic(val date:Date,
               val GPS:String,
               val description:String) : Serializable {
}

