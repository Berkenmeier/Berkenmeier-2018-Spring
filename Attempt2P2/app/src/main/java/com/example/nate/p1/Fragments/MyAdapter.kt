package com.example.nate.p1.Fragments

import android.graphics.Bitmap
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.nate.p1.Pic
import com.example.nate.p1.PicManager.getPic
import com.example.nate.p1.PicManager.returnPicList
import com.example.nate.p1.R
import com.example.nate.p1.R.id.imageView

/**
 * Created by Nate on 4/11/2018.
 */
class MyAdapter(pics:MutableList<Bitmap>) :
        RecyclerView.Adapter<MyAdapter.ViewHolder>() {

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    class ViewHolder(val imageView: ImageView) : RecyclerView.ViewHolder(imageView)


    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): MyAdapter.ViewHolder {
        // create a new view
        val imageView = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_recycler_view, parent, false) as ImageView
        // set the view's size, margins, paddings and layout parameters

        return ViewHolder(imageView)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.imageView.setImageBitmap(getPic(position))
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = returnPicList().size
}