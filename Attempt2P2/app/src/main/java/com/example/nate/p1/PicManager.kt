package com.example.nate.p1

import android.graphics.Bitmap

/**
 * Created by Nate on 3/7/2018.
 */
object PicManager {
    val PicList = mutableListOf<Bitmap>()
    val dataList = mutableListOf<Pic>()

    fun addPic(pic:Bitmap, key:Int) {
        PicList.add(key, pic)
    }
    fun getPic(index:Int): Bitmap {
        return PicList.get(index)
    }
    fun addData(pic:Pic, index: Int) {
        dataList.add(index, pic)
    }
    fun getData(index:Int): Pic {
        return dataList.get(index)
    }

    fun returnPicList(): MutableList<Bitmap> {
        return PicList
    }
}