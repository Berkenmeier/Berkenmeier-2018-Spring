package com.example.nate.p1

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_main.*
import android.provider.AlarmClock.EXTRA_MESSAGE
import android.content.Intent


import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.content.ContentValues
import android.net.Uri
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.app.FragmentTransaction
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.View
import android.widget.*
import com.example.nate.p1.Fragments.RecyclerViewFragment
import kotlinx.android.synthetic.main.content_main.*
import java.io.ByteArrayOutputStream
import java.util.*


class MainActivity : AppCompatActivity(), RecyclerViewFragment.OnFragmentInteractionListener {

    companion object {
        const val DATA_KEY:String = "data"
        const val PICTURE_KEY:String = "picture"
    }


    lateinit var recyclerFragment:RecyclerViewFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        recyclerFragment = RecyclerViewFragment.newInstance("i'm", "created")

        supportFragmentManager
                .beginTransaction()
                .add(R.id.recycler_container, recyclerFragment)
                .addToBackStack(recyclerFragment.toString())
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit()

        var picCounter:Int = 0
        var dataCounter:Int = 0
        var DB:DatabaseHandler = DatabaseHandler(this)

        val cameraButton:Button = findViewById(R.id.cameraButton)

        cameraButton.setOnClickListener {

            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.recycler_container, recyclerFragment)
                    .addToBackStack(recyclerFragment.toString())
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()

            val cameraCheckPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA )

            if( cameraCheckPermission != PackageManager.PERMISSION_GRANTED){
                if( ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) == true){
                    val builder = AlertDialog.Builder(this)

                    builder.setMessage("I need to see you to work properly!!")
                            .setTitle("Permission required")
                            .setPositiveButton("OK") { dialog, id ->
                                requestPermission()
                            }
                    val dialog = builder.create()
                    dialog.show()

                }
                else{
                    requestPermission()

                }
            }
            else{
                if(picCounter >= 4) {
                    helperText.text = "all pics taken!!!!"
                }
                else {
                    launchCamera(picCounter)
                    picCounter++
                }

            }

        }

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }

        //val firstPic = findViewById<ImageView>(R.id.firstPic)
        //val secondPic = findViewById<ImageView>(R.id.secondPic)
        //val thirdPic = findViewById<ImageView>(R.id.thirdPic)
        //val fourthPic = findViewById<ImageView>(R.id.fourthPic)
        val dataButton = findViewById<Button>(R.id.dataButton)
        val editLocation:EditText = findViewById<EditText>(R.id.editLocation)
        val editDescription:EditText = findViewById<EditText>(R.id.editDescription)
        val helperText = findViewById<TextView>(R.id.helperText)

        dataButton.setOnClickListener {
            val pic:Pic = Pic(
                    Date(),
                    editLocation.text.toString(),
                    editDescription.text.toString()
                    )
            PicManager.addData(pic, dataCounter)
            var x = convert(PicManager.getPic(dataCounter))
            var a = Date().toString()
            var b = editLocation.text.toString()
            var c = editDescription.text.toString()
            var values = ContentValues()
            values.put("id", dataCounter)
            values.put("image", x)
            values.put("date", a)
            values.put("caption", b)
            values.put("description", c)
            var result = DB.addPicture(values)
            if (result =="ok") {
                Log.e("result", "pic data added")
            } else {
                Log.e("result", "pic data unable to add")
            }
            dataCounter += 1
        }
//        firstPic.setOnClickListener {
//            if (dataCounter >=1 && picCounter >=1) {
//                val intent:Intent = Intent(this, PicViewActivity::class.java)
//                intent.putExtra(DATA_KEY, 0)
//                helperText.text = ""
//                startActivityForResult(intent, 12)
//            }
//            else {
//                helperText.text = "YOU MUST ENTER DATA FOR PICTURE 1!"
//            }
//        }
//        secondPic.setOnClickListener {
//            if (dataCounter >=2 && picCounter>=2) {
//                val intent:Intent = Intent(this, PicViewActivity::class.java)
//                intent.putExtra(DATA_KEY, 1)
//                helperText.text = ""
//                startActivityForResult(intent, 12)
//            }
//            else {
//                helperText.text = "YOU MUST ENTER DATA FOR PICTURE 2!"
//            }
//        }
//        thirdPic.setOnClickListener {
//            if (dataCounter >=3 && picCounter>=3) {
//                val intent:Intent = Intent(this, PicViewActivity::class.java)
//                intent.putExtra(DATA_KEY, 2)
//                helperText.text = ""
//                startActivityForResult(intent, 12)
//            }
//            else {
//                helperText.text = "YOU MUST ENTER DATA FOR PICTURE 3!"
//            }
//        }
//        fourthPic.setOnClickListener {
//            if (dataCounter >=4 && picCounter>=4) {
//                val intent:Intent = Intent(this, PicViewActivity::class.java)
//                intent.putExtra(DATA_KEY, 3)
//                helperText.text = ""
//                startActivityForResult(intent, 12)
//            }
//            else {
//                helperText.text = "YOU MUST ENTER DATA FOR PICTURE 4!"
//            }
//        }
    }

    private fun convert(image: Bitmap): ByteArray {
        val stream = ByteArrayOutputStream()
        image.compress(Bitmap.CompressFormat.PNG, 90, stream)
        return stream.toByteArray()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == 9090){
            if( data != null ) {
                    val imageData:Bitmap = data.extras.get("data") as Bitmap
                    //val imageView = findViewById<ImageView>(R.id.firstPic)
                    PicManager.addPic(imageData, 0)
                    //imageView.setImageBitmap(imageData)
            }
        }
        if(requestCode == 9091){
            if( data != null ) {
                val imageData:Bitmap = data.extras.get("data") as Bitmap
                //val imageView = findViewById<ImageView>(R.id.secondPic)
                PicManager.addPic(imageData, 1)
                //imageView.setImageBitmap(imageData)
            }
        }
        if(requestCode == 9092){
            if( data != null ) {
                val imageData:Bitmap = data.extras.get("data") as Bitmap
                //val imageView = findViewById<ImageView>(R.id.thirdPic)
                PicManager.addPic(imageData, 2)
                //imageView.setImageBitmap(imageData)
            }
        }
        if(requestCode == 9093){
            if( data != null ) {
                val imageData:Bitmap = data.extras.get("data") as Bitmap
                //val imageView = findViewById<ImageView>(R.id.fourthPic)
                PicManager.addPic(imageData, 3)
                //imageView.setImageBitmap(imageData)
            }
        }
    }

    private fun launchCamera(counter:Int){

        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if(counter == 0) {
            startActivityForResult(intent, 9090)
        }
        if(counter == 1) {
            startActivityForResult(intent, 9091)
        }
        if(counter == 2) {
            startActivityForResult(intent, 9092)
        }
        if(counter == 3) {
            startActivityForResult(intent, 9093)
        }

    }
    private fun requestPermission(){
        ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.CAMERA), 765)

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        for ((index, permission) in permissions.withIndex()){
            if( permission == Manifest.permission.CAMERA){
                if( grantResults[index] == PackageManager.PERMISSION_GRANTED){
                    //launchCamera()
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onFragmentInteraction(uri: Uri) {
        Log.d("hello", "fragment interaction")
    }
}
