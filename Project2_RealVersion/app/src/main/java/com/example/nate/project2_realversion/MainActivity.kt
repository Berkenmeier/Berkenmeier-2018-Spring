package com.example.nate.project2_realversion

import android.Manifest
import android.app.FragmentTransaction
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Picture
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.design.widget.Snackbar
import android.support.design.widget.NavigationView
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.example.nate.project2_realversion.dummy.PictureContent
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import java.io.ByteArrayOutputStream
import java.util.*

class MainActivity : AppCompatActivity(),
        NavigationView.OnNavigationItemSelectedListener,
        PictureItemFragment.OnListFragmentInteractionListener {

    companion object {
        const val DATA_KEY:String = "data"
    }

    lateinit var helperText: TextView
    lateinit var cameraButton: Button
    lateinit var dataButton: Button
    lateinit var editCaption: EditText
    lateinit var editDescription: EditText
    private lateinit var fragmentContainer:LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        var picCounter:Int = 0
        var dataCounter:Int = 0
        var DB:DatabaseHandler = DatabaseHandler(this)

        fragmentContainer = findViewById(R.id.fragmentContainer)
        cameraButton = findViewById(R.id.cameraButton)
        dataButton = findViewById(R.id.dataButton)
        dataButton.isClickable = false
        editCaption = findViewById(R.id.editCaption)
        editDescription = findViewById(R.id.editDescription)
        helperText = findViewById(R.id.helperText)

        //save data and insert into database onClick handler
        dataButton.setOnClickListener {
            cameraButton.isClickable = true
                val pic:Pic = Pic(
                        Date(),
                        editCaption.text.toString(),
                        editDescription.text.toString()
                )
                PicManager.addData(pic, dataCounter)
                var a = Date().toString()
                var b = editCaption.text.toString()
                var c = editDescription.text.toString()
                var x = convert(PicManager.getPic(dataCounter))
                var values = ContentValues()
                values.put("image", x)
                values.put("id", dataCounter)
                values.put("date", a)
                values.put("caption", b)
                values.put("description", c)
                var result = DB.addPicture(values)
                if (result =="ok") {
                    Log.e("result", "pic data added")
                } else {
                    Log.e("result", "pic data unable to add")
                }
                dataCounter += 1

            helperText.text = "Please take a picture!"
            dataButton.isClickable = false
            if(dataCounter == 10) {
                helperText.text = "Can Only Take 4 Pictures... For Now..."
                cameraButton.isClickable = false

            }
        }

        cameraButton.setOnClickListener {

            val cameraCheckPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA )

            if( cameraCheckPermission != PackageManager.PERMISSION_GRANTED){
                if( ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) == true){
                    val builder = AlertDialog.Builder(this)

                    builder.setMessage("I need to see you to work properly!!")
                            .setTitle("Permission required")
                            .setPositiveButton("OK") { dialog, id ->
                                requestPermission()
                            }
                    val dialog = builder.create()
                    dialog.show()

                }
                else{
                    requestPermission()

                }
            }
            else{
                    launchCamera(picCounter)
                    picCounter++
                    cameraButton.isClickable = false
                    dataButton.isClickable = true
                helperText.text = "Please Save Data!"

            }

        }

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == 9090){
            if( data != null ) {
                val imageData: Bitmap = data.extras.get("data") as Bitmap
                PicManager.addPic(imageData, 0)
            }
        }
        if(requestCode == 9091){
            if( data != null ) {
                val imageData: Bitmap = data.extras.get("data") as Bitmap
                PicManager.addPic(imageData, 1)
            }
        }
        if(requestCode == 9092){
            if( data != null ) {
                val imageData: Bitmap = data.extras.get("data") as Bitmap
                PicManager.addPic(imageData, 2)
            }
        }
        if(requestCode == 9093){
            if( data != null ) {
                val imageData: Bitmap = data.extras.get("data") as Bitmap
                PicManager.addPic(imageData, 3)
            }
        }
        if(requestCode == 9094){
            if( data != null ) {
                val imageData: Bitmap = data.extras.get("data") as Bitmap
                PicManager.addPic(imageData, 4)
            }
        }
        if(requestCode == 9095){
            if( data != null ) {
                val imageData: Bitmap = data.extras.get("data") as Bitmap
                PicManager.addPic(imageData, 5)
            }
        }
        if(requestCode == 9096){
            if( data != null ) {
                val imageData: Bitmap = data.extras.get("data") as Bitmap
                PicManager.addPic(imageData, 6)
            }
        }
        if(requestCode == 9097){
            if( data != null ) {
                val imageData: Bitmap = data.extras.get("data") as Bitmap
                PicManager.addPic(imageData, 7)
            }
        }
        if(requestCode == 9098){
            if( data != null ) {
                val imageData: Bitmap = data.extras.get("data") as Bitmap
                PicManager.addPic(imageData, 8)
            }
        }
        if(requestCode == 9099){
            if( data != null ) {
                val imageData: Bitmap = data.extras.get("data") as Bitmap
                PicManager.addPic(imageData, 9)
            }
        }
        if(requestCode == 9100){
            if( data != null ) {
                val imageData: Bitmap = data.extras.get("data") as Bitmap
                PicManager.addPic(imageData, 10)
            }
        }
    }

    private fun convert(image: Bitmap): ByteArray {
        val stream = ByteArrayOutputStream()
        image.compress(Bitmap.CompressFormat.PNG, 90, stream)
        return stream.toByteArray()
    }


    private fun launchCamera(counter:Int){

        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if(counter == 0) {
            startActivityForResult(intent, 9090)
        }
        if(counter == 1) {
            startActivityForResult(intent, 9091)
        }
        if(counter == 2) {
            startActivityForResult(intent, 9092)
        }
        if(counter == 3) {
            startActivityForResult(intent, 9093)
        }
        if(counter == 4) {
            startActivityForResult(intent, 9094)
        }
        if(counter == 5) {
            startActivityForResult(intent, 9095)
        }
        if(counter == 6) {
            startActivityForResult(intent, 9096)
        }
        if(counter == 7) {
            startActivityForResult(intent, 9097)
        }
        if(counter == 8) {
            startActivityForResult(intent, 9098)
        }
        if(counter == 9) {
            startActivityForResult(intent, 9099)
        }
        if(counter == 10) {
            startActivityForResult(intent, 9100)
        }

    }

    private fun requestPermission(){
        ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.CAMERA), 765)

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        for ((index, permission) in permissions.withIndex()){
            if( permission == Manifest.permission.CAMERA){
                if( grantResults[index] == PackageManager.PERMISSION_GRANTED){
                    //launchCamera()
                }
            }
        }
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_camera -> {
                // Handle the camera action
            }
            R.id.nav_gallery -> {

            }
            R.id.nav_slideshow -> {

            }
            R.id.nav_manage -> {

            }
            R.id.nav_share -> {

            }
            R.id.nav_send -> {

            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onListFragmentInteraction(item: PictureContent.PictureItem) {
        Log.d("BSU", "${item.id}: ${item.content}")

//        supportFragmentManager.beginTransaction()
//                .replace(R.id.fragment, PictureItemFragment.newInstance(item.id.toInt(), "hello world"))
//                .addToBackStack(fragmentContainer.toString())
//                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
//                .commit()

                val intent = Intent(this, PicViewActivity::class.java)
                intent.putExtra(DATA_KEY, (item.id.toInt()-1))
                startActivity(intent)
    }

}
