package com.example.nate.project2_realversion

import android.graphics.Bitmap

object PicManager {
    val PicList = mutableListOf<Bitmap>()
    val dataList = mutableListOf<Pic>()

    fun addPic(pic: Bitmap, key:Int) {
        PicList.add(key, pic)
    }
    fun getPic(index:Int): Bitmap {
        return PicList.get(index)
    }
    fun addData(pic:Pic, index: Int) {
        dataList.add(index, pic)
    }
    fun getData(index:Int): Pic {
        return dataList.get(index)
    }

    fun returnPicList(): MutableList<Bitmap> {
        return PicList
    }
}