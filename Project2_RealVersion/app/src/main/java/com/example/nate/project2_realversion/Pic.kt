package com.example.nate.project2_realversion

import java.io.Serializable
import java.util.*

data class Pic(val date: Date,
               val caption:String,
               val description:String) : Serializable {
}