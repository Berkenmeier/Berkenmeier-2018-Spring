package com.example.nate.project2_realversion

import android.Manifest
import android.content.ClipDescription
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.media.Image
import android.os.Bundle
import android.provider.MediaStore
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView

import kotlinx.android.synthetic.main.activity_pic_view.*
import kotlinx.android.synthetic.main.content_main.*
import java.io.ByteArrayOutputStream
import java.util.*

class PicViewActivity : AppCompatActivity() {


    lateinit var newText:TextView
    lateinit var dateView:TextView
    lateinit var captionView:TextView
    lateinit var descriptionView:TextView
    lateinit var editButtonPic:Button
    lateinit var picView:ImageView
    lateinit var editCaptionPic:EditText
    lateinit var editDescriptionPic:EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pic_view)
        var DB: DatabaseHandler = DatabaseHandler(this)
        val key: Int = intent.getIntExtra(MainActivity.DATA_KEY, Int.MAX_VALUE)

        if (Int.MAX_VALUE == key) {
            Log.d("MZ", "Intent passed no display Integer")
        }

        newText = findViewById(R.id.picViewText)

        val size: Int = PicManager.returnPicList().size
        if (key < size) {

            val image: Bitmap = PicManager.getPic(key)
            val pic: Pic = PicManager.getData(key)
            val dateView: TextView = findViewById(R.id.dateView)
            val captionView: TextView = findViewById(R.id.captionView)
            val descriptionView: TextView = findViewById(R.id.descriptionView)
            val editButtonPic: Button = findViewById(R.id.editButtonPic)
            val picView: ImageView = findViewById(R.id.picImageView)

            picView.setImageBitmap(image)
            dateView.text = pic.date.toString()
            captionView.text = pic.caption
            descriptionView.text = pic.description

            val editCaptionPic: EditText = findViewById<EditText>(R.id.editCaptionPic)
            val editDescriptionPic: EditText = findViewById<EditText>(R.id.editDescriptionPic)

            editButtonPic.setOnClickListener {
                val pic: Pic = Pic(
                        Date(),
                        editCaptionPic.text.toString(),
                        editDescriptionPic.text.toString()
                )

                PicManager.addData(pic, key)
                descriptionView.text = editDescriptionPic.text.toString()
                dateView.text = Date().toString()
                captionView.text = editCaptionPic.text.toString()
                var a = Date().toString()
                var x = convert(image)
                var b = editCaptionPic.text.toString()
                var c = editDescriptionPic.text.toString()
                var values = ContentValues()
                values.put("date", a)
                values.put("image", x)
                values.put("caption", b)
                values.put("description", c)
                var result = DB.updatePicture(values, key)
                if (result == "ok") {
                    Log.e("result", "pic data updated")
                } else {
                    Log.e("result", "pic data unable to update")
                }

                val cameraEdit: Button = findViewById(R.id.cameraEdit)

                cameraEdit.setOnClickListener {

                    val cameraCheckPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)

                    if (cameraCheckPermission != PackageManager.PERMISSION_GRANTED) {
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) == true) {
                            val builder = AlertDialog.Builder(this)

                            builder.setMessage("I need to see you to work properly!!")
                                    .setTitle("Permission required")
                                    .setPositiveButton("OK") { dialog, id ->
                                        requestPermission()
                                    }
                            val dialog = builder.create()
                            dialog.show()
                        } else {
                            requestPermission()
                        }
                    } else {
                        launchCamera(key)
                    }
                }
            }
        } else {
            newText.text = "Please Take a Picture and Fill Out Data For this Index!!!" +
                    "Click Back to Return and Do So!!!"
//            returnButton = findViewById(R.id.cameraButton)
//            returnButton.text = "RETURN"
//            returnButton.setOnClickListener {
//                val intent = Intent(this, MainActivity::class.java)
//                intent.putExtra("return", key)
//                startActivity(intent)
//            }
        }
    }

    private fun convert(image: Bitmap): ByteArray {
        val stream = ByteArrayOutputStream()
        image.compress(Bitmap.CompressFormat.PNG, 90, stream)
        return stream.toByteArray()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == 9090){
            if( data != null ) {
                val imageData:Bitmap = data.extras.get("data") as Bitmap
                val newView = findViewById<ImageView>(R.id.imageView)
                PicManager.addPic(imageData, 0)
                newView.setImageBitmap(imageData)
            }
        }
        if(requestCode == 9091){
            if( data != null ) {
                val imageData:Bitmap = data.extras.get("data") as Bitmap
                val newView = findViewById<ImageView>(R.id.imageView)
                PicManager.addPic(imageData, 1)
                newView.setImageBitmap(imageData)
            }
        }
        if(requestCode == 9092){
            if( data != null ) {
                val imageData:Bitmap = data.extras.get("data") as Bitmap
                val newView = findViewById<ImageView>(R.id.imageView)
                PicManager.addPic(imageData, 2)
                newView.setImageBitmap(imageData)
            }
        }
        if(requestCode == 9093){
            if( data != null ) {
                val imageData:Bitmap = data.extras.get("data") as Bitmap
                val newView = findViewById<ImageView>(R.id.imageView)
                PicManager.addPic(imageData, 3)
                newView.setImageBitmap(imageData)
            }
        }
        if(requestCode == 9094){
            if( data != null ) {
                val imageData:Bitmap = data.extras.get("data") as Bitmap
                val newView = findViewById<ImageView>(R.id.imageView)
                PicManager.addPic(imageData, 4)
                newView.setImageBitmap(imageData)
            }
        }
        if(requestCode == 9095){
            if( data != null ) {
                val imageData:Bitmap = data.extras.get("data") as Bitmap
                val newView = findViewById<ImageView>(R.id.imageView)
                PicManager.addPic(imageData, 5)
                newView.setImageBitmap(imageData)
            }
        }
        if(requestCode == 9096){
            if( data != null ) {
                val imageData:Bitmap = data.extras.get("data") as Bitmap
                val newView = findViewById<ImageView>(R.id.imageView)
                PicManager.addPic(imageData, 6)
                newView.setImageBitmap(imageData)
            }
        }
        if(requestCode == 9097){
            if( data != null ) {
                val imageData:Bitmap = data.extras.get("data") as Bitmap
                val newView = findViewById<ImageView>(R.id.imageView)
                PicManager.addPic(imageData, 7)
                newView.setImageBitmap(imageData)
            }
        }
        if(requestCode == 9098){
            if( data != null ) {
                val imageData:Bitmap = data.extras.get("data") as Bitmap
                val newView = findViewById<ImageView>(R.id.imageView)
                PicManager.addPic(imageData, 8)
                newView.setImageBitmap(imageData)
            }
        }
        if(requestCode == 9099){
            if( data != null ) {
                val imageData:Bitmap = data.extras.get("data") as Bitmap
                val newView = findViewById<ImageView>(R.id.imageView)
                PicManager.addPic(imageData, 9)
                newView.setImageBitmap(imageData)
            }
        }
        if(requestCode == 9100){
            if( data != null ) {
                val imageData:Bitmap = data.extras.get("data") as Bitmap
                val newView = findViewById<ImageView>(R.id.imageView)
                PicManager.addPic(imageData, 10)
                newView.setImageBitmap(imageData)
            }
        }
    }

    private fun launchCamera(counter:Int){

        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if(counter == 0) {
            startActivityForResult(intent, 9090)
        }
        if(counter == 1) {
            startActivityForResult(intent, 9091)
        }
        if(counter == 2) {
            startActivityForResult(intent, 9092)
        }
        if(counter == 3) {
            startActivityForResult(intent, 9093)
        }
        if(counter == 4) {
            startActivityForResult(intent, 9094)
        }
        if(counter == 5) {
            startActivityForResult(intent, 9095)
        }
        if(counter == 6) {
            startActivityForResult(intent, 9096)
        }
        if(counter == 7) {
            startActivityForResult(intent, 9097)
        }
        if(counter == 8) {
            startActivityForResult(intent, 9098)
        }
        if(counter == 9) {
            startActivityForResult(intent, 9099)
        }
        if(counter == 10) {
            startActivityForResult(intent, 9100)
        }

    }

    private fun requestPermission(){
        ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.CAMERA), 765)

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        for ((index, permission) in permissions.withIndex()) {
            if (permission == Manifest.permission.CAMERA) {
                if (grantResults[index] == PackageManager.PERMISSION_GRANTED) {
                    //launchCamera()
                }
            }
        }
    }
}

