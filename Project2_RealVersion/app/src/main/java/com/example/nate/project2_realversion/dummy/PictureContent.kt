package com.example.nate.project2_realversion.dummy

import com.example.nate.project2_realversion.PicManager
import java.util.ArrayList
import java.util.HashMap

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 *
 * TODO: Replace all uses of this class before publishing your app.
 */
object PictureContent {

    /**
     * An array of sample (dummy) items.
     */
    val ITEMS: MutableList<PictureItem> = ArrayList()

    /**
     * A map of sample (dummy) items, by ID.
     */
    val ITEM_MAP: MutableMap<String, PictureItem> = HashMap()

    private val COUNT = 10
    //could make count size of the pic manager of pics? and set each item to the pic in the array of pictures?

    init {
        // Add some sample items.
        for (i in 1..COUNT) {
            addItem(createPictureItem(i))
        }
    }

    private fun addItem(item: PictureItem) {
        ITEMS.add(item)
        ITEM_MAP.put(item.id, item)
    }

    private fun createPictureItem(position: Int): PictureItem {
//        var aob: String = "iVBORw0KGgoAAAANSUhEUgAAAFoAAACgCAIAAAADw+wqAAAAA3NCSVQICAjb4U/gAAAKvUlEQVR4nO1dXctdRxV+1syc8ybmTWhDm5SWCCJoU6tYNRGt4F1rQMGf4IV4Ua961R8geBfwqhQUb3onGEQrgt4o6lUjmFo/KAGLJFCagiQxX+/eM8uL2V+zZ/Y5e87+ePd7zn6ukgcymVl77VnPrLVmH+KHPwMASECCDYCfX//Fuzf+yczMrJQSQrCHgr945s5LzwoAIAYACLACGTADpiOv9VUpBUZE8Z8lAIMIAFWQpqkxhjwUfDkI2z8bkAELEPXAjw4BFmABCCABDEg0rTzMQ3orSUFpaIXx/OiouWICMFwHWWuR3nwhwI8Nlf/3AAACkDBz/haUSNPU7hc13vABqHj/BWDABGIg7YEfHfaBONBar94vauh5v6jyo0PY/QIQxT7SZget2MLOu7/9osaPC1GNKRatdtCqRYaIKRxw2xHgxJT8r3EWqaykbx8ZHbPucDDrDgfK/WsCiL29vf39/aokByClfPDgQe0f57rDj5rFM3d5YkgCLwANSFeVB3jSY28fxA+uAMhnYP2zPL9U+R/84bJ/fvny2duXnj/W8jyi9eeleBOHo7BaIbiB12ONRQ+645DURHsowIBkpkor2hSQNd7uF542LXRHG605dQR0R462eiQmdkz4PQEQ1h1erLF8d91xGMIqDgHd4WKNHikHaaUjpm4Plb3SDftFlSfXWHYfyXRHyzNrNuh0Ecp3NPhIkxJrrzWZ9cDL6YpAvqPJR4wxXfMdk8esOxzMusPBrDsczLrDwaw7HHTTHQvBRHG6Y9oGCeQ7QARmn7/0mZeL9Adg7B+e+dhJyCfLo70FMVjm45S8oLPQk3aRiHxHmOfzoOdHnfKQiNAdYf4ohM/2iNAdjfwWIUJ3rOO3ASq3SGtfcHgzfd0dhQjdEeLlsLMbHRG6o4HfqrcmIt8R5iefwohCRL4jzB9G6XA4zLrDwaw7HMy6w8GsOxzMusNBQHccPDpIkgRk2ysMcA+siCQAZq7yjOVCPFJLA+R7ahFobDdDO16IqSTKAvmOt2789l83/6Ebuq5r+OrT918+fwxkSgFi+zug8gzQep7xCswrU8iDhHQHa7+xtLGv1Ng8qAIhS/YwgWxDYnt+hJW2wqT6Sg8fk+krnQYm0lc6FUyjrxRTySdPoq+UJqNf1N9vvgfAFgqIpCB1+97/qOLATTFlk/6OBt6Y/4Df5no1AwCIZL0Ngk5Kcb5vO2RQV67/ytcX5L7PKywS11fawAtxBXgrG9QunooCTSEFcp4vAj8dyhztVz7kfRYBpHkfKsAMNoAsjeLwA24z9ZbrqJiSb6WD3WcZ/Z6LiFr5IdxnCfBDmiPWF8a9zzK2TonToEG+MuNxfKR27OzbHF0sYgepzHhoH0mBZDhzlJElNqas1h0f/ZEe3tLMDMp6IJhTQbLIErThn/qm2dvb92LQgBlaJ9BuYJEG3ZGm7zyu/71ccVu9DZ9848beMU+nDImBdIewWYJYX/P5gE4ZUnfUzbFiZkGeHN1RWoTCXZdx41MWxWvjDwjV+RkWuqPyDEmjsQ81cvyAlt14seuhOs440x3180gZg7uNn8cUZ3waziKq4zMsdYfzDFFKkh4s4p13BoPq6RnWz6xVc2w8fjiKD6o73r+wD8BAA5CkBAsADJ33B67hPxTq90kCGA0rFjQAIdS3WT3l7tEbWISRgE94uuNdY74DliCTp0iyl4eIajyTUeLNCHPcPQkDgMgwA+mCFJhAxMxt+Lu0AC8BMIwBQMYwg8w9+CEr2iLsxywAfF/Iq1nBLHBb1eM5IjQLghAAWAhIAAmnICamOB6o8qb5tkcUb23lnF/KoswgOfrKdgjY0mDCKRN34SVkl3NQxR413TH42VfYZy4bfGFDvvPJsPSOrmdiGSViRXdf8HliNK0wiu/nTBwDge6+4O8vG/mCzxcz7OYjEeZQH926xQAzL0gRBIi1PV9D1/jTTzxhfcEwJ5zFGsqvN1R5UVGlFv3pjrgcPZBGvSzqL598rzhQSymllP7p2/KX/nuGYQwLARhou3JikqAaL0kR6pG2J90Rm6OPC7TOiVZrDUDKek3M8sh0Tt0XfB/RnBquF2s2sEhAd8T6SGSsrR/wV1kk2y/qvuD7SEpJmtqj8gD5jigf4bi210C+o8kiq/eLKi8hicxg+Y4oH4myBoTW2t/btdY+j13QHcGVBy2yE7ojuPKgRXZBd4jgyoMWafIFnxd9WSTwFa74ukyU7ijWszrKSiklxKw7Sh6z7nD4WXdUMesOB5h1R5WZdcfO6Q714o++xMxMUGJBRFlXAcCsa3xhv23Odzz23bLOYmMEF1WCIL/VuiObRF85dMtrTg03fmesPc8b7xdlVSFy7+g5h27rLGSGqrMgch+JbI+Z6yw1c8x1lqo55jqLYw67EkMMJkkk3BU6/C7ojme+9wIza/BClN2MufRw+Jtv/BVbrzvOXD/XssvxA7yz9brDOdEmOl0gMDPLY9fyHYLDM7P8LuQ7AvdZEh3e4XZCd/gzE2jUiDugO5p8weN3Qnc0+YLP74LuEE2+4PNNvuDzR7fOkpmjjY+0P78Y20vY2SKMZPOzbMFHqVKqbL+z7ph1h4NZd7jmmHWHY45Zd1Sh5ItJgoOlVFkNjxnMBBCzx68/y1qehr7PErWPxNyeU9d++Dub3cjvJOR9pfk1iIK3EWu78x2CGt7/Jn67dcdcZ3HNMddZHHP4z3aus8x1ltwcc52lCvXnV9+GvQtpn3eO8o5kzn/l8hdH1h2//M1pm9kHsjuYRAspZdZsUeEhFl+7cOfcxxdddcfdnzxqc3uDmXEZGFd3XP3b59I0bZpPDZ/6xLVz5zrXWYpJT7CvtJITC8ynxjN0/q3LLe0rTdN0xXx8fsv7Su3O2jgfl88XssV9pUREtPYtLqnOfaVKa93eG9fGFMtrCCKmzt/vKPbjVhbhPMq634qINsf6/wlAu5hiecoPLO1XHuSr4WntPHOd4p1xo8wR4Y2j5ztq0Xr1PMkK0I66I8IbAYyc73AHWT1PY1LwctYdGWbdMeuO5nnOumPWHSvm2YvuOH36dMvvAI6vO06dOmWMqZ5li/mgkouw/HK5BHRX3XHh/fNoe1cBI+uO177/6xMnTgDVLx0V+wKH+NA3uWN0h+g3h37k+zv6zaFjrrP4/FxnmessW1lnabqT0JbPLQJiMjKr78b/qkmQr6xwpDqLWty6t8BSCMHQOo/nAAweteEfP46zjykADK0AgmSQAe1LTZ11R7nCTWr3G+mOZ69p5vsNSmw9f/HMnZeOC6CuCz7gE/fznxk5Qv0dQ30nffP9osof4vc7+utISUGCSJK7k20w/iHfZ9ksQ9PwCzQp0bL9OFPsK91gxofwnfQh+0qP4nfSo3wkyhoDfScd9vu/PVikU0zZIN/RPUMTfCZPfn1hXhAAmFlKu9dmyoVhWvK0d7xikVF0R8dnWGpH95ksPns7Jk/RxC8qvzt5lHVHZ73Q3++5ROU7BlBK3c6gvfMb3HbqYpEp+ULnfEfs21GD4YNp+ULHfEf02+FhQr4wxH2WKIvk43jPqonv/syLegoMcJD9tjblO0Sdj8t30Id3XydaZPEfthablRZq/Ot/+rFffzkuH+7LJK93FLz51nPy6VMmn2Xr30NoxX8a4rXKEpoOJiZLDqRfaG+R/wOGTSZMfner8QAAAABJRU5ErkJggg=="
//        return UserDto((position).toString(), "Restaurant " + position, makeDetails(position), stringToByte(aob)
        return PictureItem(position.toString(), "Picture: " + position, makeDetails(position))
    }

    private fun makeDetails(position: Int): String {
        val builder = StringBuilder()
        builder.append("Details about Picture: ").append(position)
        for (i in 0..position - 1) {
            //builder.append(PicManager.getData(position)) haven't created details, so what about null pointers
        }
        return builder.toString()
    }

    /**
     * A dummy item representing a piece of content.
     */
    data class PictureItem(val id: String, val content: String, val details: String) {
        override fun toString(): String = content
    }
}
