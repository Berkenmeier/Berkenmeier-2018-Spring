package com.example.nate.p2

import java.io.Serializable
import java.util.*

data class Pic(val date: Date,
               val caption:String,
               val description:String) : Serializable {
}