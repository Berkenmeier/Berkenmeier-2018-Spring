package com.example.nate.homework1

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView


class MainActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //model this like the SetupUI function in the example code for all variables?
        val concat:Button = findViewById(R.id.concat)
        val add:Button = findViewById(R.id.add)
        val dynamic:TextView = findViewById(R.id.dynamicText)
        val text1:EditText = findViewById(R.id.editText)
        val text2:EditText = findViewById(R.id.editText2)

        concat.setOnClickListener {
            dynamic.text = text1.text
            dynamic.append(" " + text2.text)
        }

        add.setOnClickListener {
            //val and var mixup
            //can do something like this instead?
//            val temp:Int = text1.toInt()
//            val temp2:Int = text2.toInt()
//            val res:Int = temp + temp2
//            dynamic.text = res.toString()
//            val temp:String = text1.toString() + text2.toString()
//            var int:Int = 0
//
//            for(i in temp.indices){
//                int += temp[i].toInt()
//            }

            val editOne:Int = try {text1.text!!.toString().toInt()}
            catch (exception:Exception) {
                Log.e( "BSU", exception.toString())
            40}
            val editTwo:Int = try {text2.text!!.toString().toInt()}
            catch (exception:Exception) {
                Log.e( "BSU", exception.toString())
            40}

            val result:Int = editOne + editTwo

            dynamic.text = result.toString()
        }

    }

}