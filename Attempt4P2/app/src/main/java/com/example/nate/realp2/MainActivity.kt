package com.example.nate.realp2

import android.Manifest
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import java.io.ByteArrayOutputStream
import java.util.*

class MainActivity : AppCompatActivity() {
    companion object {
        const val DATA_KEY:String = "data"
        const val PICTURE_KEY:String = "picture"
    }

    var picCounter:Int = 0
    var dataCounter:Int = 0
    var DB:DatabaseHandler = DatabaseHandler(this)
    val helperText = findViewById<TextView>(R.id.helperText)
    val dataButton = findViewById<Button>(R.id.dataButton)
    val editCaption: EditText = findViewById<EditText>(R.id.editCaption)
    val editDescription: EditText = findViewById<EditText>(R.id.editDescription)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val cameraButton:Button = findViewById(R.id.cameraButton)

        cameraButton.setOnClickListener {

            val cameraCheckPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA )

            if( cameraCheckPermission != PackageManager.PERMISSION_GRANTED){
                if( ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) == true){
                    val builder = AlertDialog.Builder(this)

                    builder.setMessage("I need to see you to work properly!!")
                            .setTitle("Permission required")
                            .setPositiveButton("OK") { dialog, id ->
                                requestPermission()
                            }
                    val dialog = builder.create()
                    dialog.show()

                }
                else{
                    requestPermission()

                }
            }
            else{
                if(picCounter >= 4) {
                    helperText.text = "All pics taken!!!!"
                }
                else {
                    launchCamera(picCounter)
                    picCounter++
                    if(picCounter == 4) {
                        cameraButton.isClickable = false
                    }
                }

            }

        }

        dataButton.setOnClickListener {
            val pic:Pic = Pic(
                    Date(),
                    editCaption.text.toString(),
                    editDescription.text.toString()
            )
            PicManager.addData(pic, dataCounter)
            var x = convert(PicManager.getPic(dataCounter))
            var a = Date().toString()
            var b = editCaption.text.toString()
            var c = editDescription.text.toString()
            var values = ContentValues()
            values.put("id", dataCounter)
            values.put("image", x)
            values.put("date", a)
            values.put("caption", b)
            values.put("description", c)
            var result = DB.addPicture(values)
            if (result =="ok") {
                Log.e("result", "pic data added")
            } else {
                Log.e("result", "pic data unable to add")
            }
            dataCounter += 1
            if(dataCounter == 4) {
                helperText.text = "All pics taken. All data collected. Click to open View Fragment!"
                dataButton.isClickable = false
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == 9090){
            if( data != null ) {
                val imageData: Bitmap = data.extras.get("data") as Bitmap
                PicManager.addPic(imageData, 0)
            }
        }
        if(requestCode == 9091){
            if( data != null ) {
                val imageData: Bitmap = data.extras.get("data") as Bitmap
                PicManager.addPic(imageData, 1)
            }
        }
        if(requestCode == 9092){
            if( data != null ) {
                val imageData: Bitmap = data.extras.get("data") as Bitmap
                PicManager.addPic(imageData, 2)
            }
        }
        if(requestCode == 9093){
            if( data != null ) {
                val imageData: Bitmap = data.extras.get("data") as Bitmap
                PicManager.addPic(imageData, 3)
            }
        }
    }

    private fun launchCamera(counter:Int){

        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if(counter == 0) {
            startActivityForResult(intent, 9090)
        }
        if(counter == 1) {
            startActivityForResult(intent, 9091)
        }
        if(counter == 2) {
            startActivityForResult(intent, 9092)
        }
        if(counter == 3) {
            startActivityForResult(intent, 9093)
        }
    }

    private fun requestPermission(){
        ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.CAMERA), 765)
    }

    private fun convert(image: Bitmap): ByteArray {
        val stream = ByteArrayOutputStream()
        image.compress(Bitmap.CompressFormat.PNG, 90, stream)
        return stream.toByteArray()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        for ((index, permission) in permissions.withIndex()){
            if( permission == Manifest.permission.CAMERA){
                if( grantResults[index] == PackageManager.PERMISSION_GRANTED){
                    //launchCamera()
                }
            }
        }
    }

}
