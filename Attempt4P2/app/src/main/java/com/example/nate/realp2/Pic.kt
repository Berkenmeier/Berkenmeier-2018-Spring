package com.example.nate.realp2

import java.io.Serializable
import java.util.*

/**
 * Created by Nate on 4/12/2018.
 */
data class Pic(val date: Date,
               val caption:String,
               val description:String) : Serializable {
}