package com.example.nate.realp2

/**
 * Created by Nate on 4/12/2018.
 */
class PicData {

    var picID:Int?=null
    var image:ByteArray?=null
    var date: String?=null
    var caption:String?=null
    var description:String?=null

    constructor(id:Int, image:ByteArray, date: String, caption:String, description:String){
        this.picID = id
        this.image = image
        this.date = date
        this.caption = caption
        this.description = description
    }
}