package com.example.nate.p1

/**
 * Created by Nate on 4/3/2018.
 */

import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.content.Context
import android.content.ContentValues
import android.database.sqlite.SQLiteQueryBuilder
import android.util.Log
import java.util.ArrayList

class DatabaseHandler : SQLiteOpenHelper {

    companion object {

        val Tag = "DatabaseHandler"
        val DBName = "ContactDB"
        val DBVersion = 1

        val tableName = "picDataTable"
        val PicID = "id"
        val Date = "date"
        val Caption = "caption"
        val Description = "description"
    }

    var context: Context? = null
    var sqlObj: SQLiteDatabase

    constructor(context: Context) : super(context, DBName, null, DBVersion) {

        this.context = context;
        sqlObj = this.writableDatabase;
    }

    override fun onCreate(p0: SQLiteDatabase?) {

        //SQL for creating table
        var sql1: String = "CREATE TABLE IF NOT EXISTS " + tableName + " " +
                "(" + PicID + " INTEGER PRIMARY KEY," +
                Date + " TEXT, " + Caption + " TEXT, " + Description + " TEXT );"

        p0!!.execSQL(sql1);
    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {

        p0!!.execSQL("Drop table IF EXISTS " + tableName)
        onCreate(p0)

    }

    fun AddContact(values: ContentValues): String {

        var Msg: String = "error";
        val ID = sqlObj!!.insert(tableName, "", values)

        if (ID > 0) {
            Msg = "ok"
        }
        return Msg
    }

    fun FetchPictures(keyword: String): ArrayList<PicData> {

        var arraylist = ArrayList<PicData>()

        val sqb = SQLiteQueryBuilder()
        sqb.tables = tableName
        val cols = arrayOf("id", "date", "caption", "description")
        val rowSelArg = arrayOf(keyword)

        val cur = sqb.query(sqlObj, cols, "caption like ?", rowSelArg, null, null, "caption")

        if (cur.moveToFirst()) {

            do {
                val id = cur.getInt(cur.getColumnIndex("id"))
                val date = cur.getString(cur.getColumnIndex("date"))
                val caption = cur.getString(cur.getColumnIndex("caption"))
                val description = cur.getString(cur.getColumnIndex("description"))


                arraylist.add(PicData(id, date, caption, description))

            } while (cur.moveToNext())
        }

        var count: Int = arraylist.size

        return arraylist
    }

    fun UpdateContact(values: ContentValues, id: Int): String {

        var selectionArs = arrayOf(id.toString())

        val i = sqlObj!!.update(tableName, values, "id=?", selectionArs)
        if (i > 0) {
            return "ok";
        } else {

            return "error";
        }
    }

    fun RemoveContact(id: Int): String {

        var selectionArs = arrayOf(id.toString())

        val i = sqlObj!!.delete(tableName, "id=?", selectionArs)
        if (i > 0) {
            return "ok";
        } else {

            return "error";
        }
    }

}