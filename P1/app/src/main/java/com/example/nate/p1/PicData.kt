package com.example.nate.p1

import java.util.*
import android.graphics.Bitmap

/**
 * Created by Nate on 4/3/2018.
 */
class PicData {

    var picID:Int?=null
    var date: String?=null
    var caption:String?=null
    var description:String?=null


    constructor(id:Int, date: String, caption:String, description:String){
        this.picID = id
        this.date = date
        this.caption = caption
        this.description = description

    }

}