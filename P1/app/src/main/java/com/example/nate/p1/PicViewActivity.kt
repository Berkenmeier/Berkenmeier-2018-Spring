package com.example.nate.p1

import android.Manifest
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.media.Image
import android.os.Bundle
import android.provider.MediaStore
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.example.nate.p1.MainActivity.Companion.DATA_KEY

import kotlinx.android.synthetic.main.activity_pic_view.*
import kotlinx.android.synthetic.main.content_main.*
import java.util.*

class PicViewActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pic_view)
        setSupportActionBar(toolbar)
        var DB:DatabaseHandler = DatabaseHandler(this)
        val key:Int = intent.getIntExtra(MainActivity.DATA_KEY, Int.MAX_VALUE)

        if( Int.MAX_VALUE == key ){
            Log.d("MZ", "Intent passed no display Integer")
        }

        val image:Bitmap = PicManager.getPic(key)
        val pic:Pic = PicManager.getData(key)
        val dateView: TextView = findViewById(R.id.dateView)
        val GPSView: TextView = findViewById(R.id.GPSView)
        val descriptionView: TextView = findViewById(R.id.descriptionView)
        val editButtonPic: Button = findViewById(R.id.editButtonPic)

        val editLocationPic: EditText = findViewById<EditText>(R.id.editLocationPic)
        val editDescriptionPic: EditText = findViewById<EditText>(R.id.editDescriptionPic)

        editButtonPic.setOnClickListener {
            val pic:Pic = Pic(
                    Date(),
                    editLocationPic.text.toString(),
                    editDescriptionPic.text.toString()
            )
            PicManager.addData(pic, key)
            descriptionView.text = editDescriptionPic.text.toString()
            dateView.text = Date().toString()
            GPSView.text = editLocationPic.text.toString()
            var a = Date().toString()
            var b = editLocationPic.text.toString()
            var c = editDescriptionPic.text.toString()
            var values = ContentValues()
            values.put("date", a)
            values.put("caption", b)
            values.put("description", c)
            var result = DB.UpdateContact(values, key)
            if (result =="ok") {
                Log.e("result", "pic data updated")
            } else {
                Log.e("result", "pic data unable to update")
            }
        }

        val view:ImageView = findViewById(R.id.imageView)

        view.setImageBitmap(image)
        dateView.text = pic.date.toString()
        GPSView.text = pic.GPS
        descriptionView.text = pic.description


        val cameraEdit:Button = findViewById(R.id.cameraEdit)

        cameraEdit.setOnClickListener {

            val cameraCheckPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA )

            if( cameraCheckPermission != PackageManager.PERMISSION_GRANTED){
                if( ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) == true){
                    val builder = AlertDialog.Builder(this)

                    builder.setMessage("I need to see you to work properly!!")
                            .setTitle("Permission required")
                            .setPositiveButton("OK") { dialog, id ->
                                requestPermission()
                            }
                    val dialog = builder.create()
                    dialog.show()

                }
                else{
                    requestPermission()

                }
            }
            else{

                    launchCamera(key)

            }

        }

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }
    }

    private fun launchCamera(counter:Int){

        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if(counter == 0) {
            startActivityForResult(intent, 9090)
        }
        if(counter == 1) {
            startActivityForResult(intent, 9091)
        }
        if(counter == 2) {
            startActivityForResult(intent, 9092)
        }
        if(counter == 3) {
            startActivityForResult(intent, 9093)
        }

    }
    private fun requestPermission(){
        ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.CAMERA), 765)

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        for ((index, permission) in permissions.withIndex()){
            if( permission == Manifest.permission.CAMERA){
                if( grantResults[index] == PackageManager.PERMISSION_GRANTED){
                    //launchCamera()
                }
            }
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == 9090){
            if( data != null ) {
                val imageData:Bitmap = data.extras.get("data") as Bitmap
                val newView = findViewById<ImageView>(R.id.imageView)
                PicManager.addPic(imageData, 0)
                newView.setImageBitmap(imageData)
            }
        }
        if(requestCode == 9091){
            if( data != null ) {
                val imageData:Bitmap = data.extras.get("data") as Bitmap
                val newView = findViewById<ImageView>(R.id.imageView)
                PicManager.addPic(imageData, 1)
                newView.setImageBitmap(imageData)
            }
        }
        if(requestCode == 9092){
            if( data != null ) {
                val imageData:Bitmap = data.extras.get("data") as Bitmap
                val newView = findViewById<ImageView>(R.id.imageView)
                PicManager.addPic(imageData, 2)
                newView.setImageBitmap(imageData)
            }
        }
        if(requestCode == 9093){
            if( data != null ) {
                val imageData:Bitmap = data.extras.get("data") as Bitmap
                val newView = findViewById<ImageView>(R.id.imageView)
                PicManager.addPic(imageData, 3)
                newView.setImageBitmap(imageData)
            }
        }
    }

}
